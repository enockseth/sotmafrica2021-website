---
layout: post
title:  SotM Africa 2021 is in Nairobi, Kenya
date:   2020-11-17 08:00:00
image:  nairobi-giraffe.jpg
isStaticPost: false
---
![](../img/posts/nairobi-giraffe.jpg)

We are pleased to announce the next State of the Map Africa which will take place from 19 to 21 November 2021 in Nairobi, Kenya.

Join OpenStreetMap and Free and Open Source Software (FOSS) entusiasts learn, share and connect in Nairobi, Kenya. And also also discuss the theme **Leaving none behind: Emerging from a global pandemic** and the future of OpenStreetMap on the Africa continent.

Stay tuned for updates.

