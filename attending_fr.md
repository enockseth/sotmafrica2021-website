---
layout: post
title: Participation à SotM Africa 2021
permalink: /fr/attending/
isStaticPost: true
image: nairobi.jpg
---

__[English Version](/attending/)__

#### À propos de Nairobi
Nairobi a été créée à la fin des années 1890 comme une colonie ferroviaire coloniale. Elle a grandi autour d'une ligne de chemin de fer construite par les fonctionnaires coloniaux britanniques de Mombasa, sur la côte de l'océan Indien, à l'Ouganda. Le site actuel de Nairobi a été choisi pour servir de dépôt de magasins, de cour de triage et de terrain de camping pour les milliers de travailleurs indiens et les colons britanniques. 
Il tire son nom de l'expression massaï Enkare nyirobi qui se traduit par "eau fraîche", une référence au fait qu'il était autrefois un marécage et à la rivière Nairobi qui traverse la ville. Cependant, elle est populairement connue sous le nom de "Ville verte au soleil".

Nairobi est la capitale et la plus grande ville du Kenya, la ville la plus autonome sur le plan économique et un important centre d'affaires en Afrique de l'Est. Le centre urbain métropolitain (1°09′S 36°39′E et 1°27′S 37°06′E) a une population estimée à 4 397 073 habitants, selon le recensement de la population de 2019. 

####  Atouts
L'observation de la vie sauvage avec en toile de fond la silhouette de la ville rend Nairobi spéciale car c'est la seule capitale au monde qui possède un parc national. À 15 minutes en voiture des gratte-ciel du centre ville, vous pouvez vivre une expérience africaine classique de la vie sauvage dans le parc national de Nairobi. Lion, guépard, zèbre, gnou, girafe, rhinocéros et buffle parcourent ici la savane ensoleillée. Les amoureux des animaux peuvent câliner et nourrir les bébés éléphants au centre de David Sheldrick, connu surtout pour la protection des bébés éléphants orphelins, et se mettre en contact avec les girafes au centre Giraffe. C'est également là que se trouvent l'orphelinat pour animaux, le village de Mamba (crocodile), la forêt de Karura, Bomas of Kenya, le musée de Nairobi et le parc aux serpents, entre autres.


#### Politique des visas pour le Kenya
La réglementation sur les visas pour se rendre au Kenya stipule que les citoyens d'environ 45 pays peuvent visiter le pays sans avoir besoin d'obtenir un visa, en fonction de la durée et de l'objet du séjour prévu au Kenya. Le Kenya n'offre pas aux citoyens étrangers la possibilité d'obtenir un visa à leur arrivée, mais permet à un maximum de 140 pays et territoires dans le monde d'obtenir un visa électronique à entrée unique par le biais d'une demande en ligne au moins 7 jours avant le voyage.
Les citoyens de 60 autres pays doivent obtenir un visa auprès d'un bureau gouvernemental, soit une ambassade ou un consulat.
Voir la liste complète des différentes catégories [ici : Politique des visas pour le Kenya](https://www.onlinevisa.com/visa-policy/kenya/)

#### Transport

####  Se rendre à Nairobi 
##### Par voie aérienne
Nairobi abrite l'aéroport international Jomo Kenyatta (JKIA), première plaque tournante de l'Afrique et escale idéale pour entrer et sortir de l'Afrique de l'Est et du Centre, ainsi qu'un centre d'activités aériennes pour la région. Il est desservi par plus de 40 compagnies aériennes de passagers, avec des liaisons directes vers l'Afrique, l'Asie, l'Europe, le Moyen-Orient et les États-Unis.


###### De l'aéroport aux hôtels 
JKIA se trouve à seulement 18 km du centre ville et on peut facilement s'y rendre en utilisant les transports publics ou les services de taxi.
Les taxis officiels de l'aéroport sont de couleur jaune. 
Cependant, les services de taxi comme Uber et Bolt sont moins chers.  


##### Bus
- Les pays voisins d'Afrique orientale, centrale et australe peuvent se rendre à Nairobi en utilisant les services de bus publics.

##### Les transports en ville
Il existe plusieurs options pour se rendre sur place et se déplacer dans Nairobi :

###### Transports publics 
Les bus et les minibus, appelés localement "matatu", sont le principal mode de transport à Nairobi. Chaque bus dessert un itinéraire spécifique, le début et la fin de chaque voyage étant le quartier central des affaires de Nairobi ; on peut donc prendre deux bus différents pour se rendre d'un point à un autre. Les voyages ne peuvent être payés qu'en espèces, et il faut connaître les arrêts de bus et l'endroit où ils prévoient de descendre.

###### Taxis
Les taxis  sont souvent chers, mais il est beaucoup plus facile d'utiliser un service mobile d'appel de taxis. Nairobi dispose de Uber, Bolt, et de sa propre option locale, Little. 


###### Taxi-motos
Les motos, communément appelées "boda boda", sont l'un des moyens les plus rapides de se déplacer dans la ville. Il est possible de réserver une moto à l'aide des applications mobiles suivantes : Uber, Bolt, Little et SafeBoda.

#### Hébergement
Ville touristique très prisée, Nairobi offre de nombreuses possibilités d'hébergement pour de courts trajets en bus, en taxi ou à pied pour se rendre sur les lieux de la conférence. 

Nous disposons d'une variété d'hôtels allant des hôtels 5 étoiles comme la Villa Rosa Kempinski, l'hôtel Intercontinental, l'auberge Boma, l'hôtel Hilton, Golden Tulip, Ibis Styles, entre autres, aux hôtels 1 étoile qui n'offrent que l'essentiel tout en respectant des normes d'hygiène et de sécurité raisonnables, le tout en fonction du budget et des préférences de chacun. 

Airbnb est disponible dans toute la ville à partir de 9 dollars par nuit et peut également accueillir des groupes plus importants. Nous disposons également d'auberges et de dortoirs fournis par la Kenya Young Men Christian Association (YMCA), allant de dortoirs partagés à des chambres individuelles ou doubles privées, et nous sommes à une distance de marche du centre de Nairobi. 

#### Alimentation
L'aliment de base et le plus courant au Kenya est un amidon de farine de maïs transformé en une pâte épaisse appelée ugali. Elle se marie bien avec les légumes frits ou tout autre type de ragoût de viande et est disponible partout, des restaurants à la rue et aux étals dans tout le pays. Outre l'ugali, la plupart des plats kenyans tiennent compte de la nature multiraciale du pays et on trouve dans les restaurants kenyans de nombreux autres plats appréciés par différents pays. Les repas sont copieux et peu coûteux, et à partir d'un dollar, on peut avoir un plat satisfaisant.